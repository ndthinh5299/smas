import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'student',
        data: { pageTitle: 'gatewayApp.studentserviceStudent.home.title' },
        loadChildren: () => import('./studentservice/student/student.module').then(m => m.StudentserviceStudentModule),
      },
      {
        path: 'teacher',
        data: { pageTitle: 'gatewayApp.teacherserviceTeacher.home.title' },
        loadChildren: () => import('./teacherservice/teacher/teacher.module').then(m => m.TeacherserviceTeacherModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
