import * as dayjs from 'dayjs';

export interface IStudent {
  id?: number;
  studentCode?: string | null;
  name?: string | null;
  birthday?: dayjs.Dayjs | null;
  address?: string | null;
  email?: string | null;
  homePhone?: string | null;
  parentId?: number | null;
}

export class Student implements IStudent {
  constructor(
    public id?: number,
    public studentCode?: string | null,
    public name?: string | null,
    public birthday?: dayjs.Dayjs | null,
    public address?: string | null,
    public email?: string | null,
    public homePhone?: string | null,
    public parentId?: number | null
  ) {}
}

export function getStudentIdentifier(student: IStudent): number | undefined {
  return student.id;
}
