import * as dayjs from 'dayjs';

export interface ITeacher {
  id?: number;
  name?: string | null;
  birthday?: dayjs.Dayjs | null;
  address?: string | null;
  email?: string | null;
  phone?: string | null;
  derpartment?: string | null;
  grade?: string | null;
  university?: string | null;
}

export class Teacher implements ITeacher {
  constructor(
    public id?: number,
    public name?: string | null,
    public birthday?: dayjs.Dayjs | null,
    public address?: string | null,
    public email?: string | null,
    public phone?: string | null,
    public derpartment?: string | null,
    public grade?: string | null,
    public university?: string | null
  ) {}
}

export function getTeacherIdentifier(teacher: ITeacher): number | undefined {
  return teacher.id;
}
