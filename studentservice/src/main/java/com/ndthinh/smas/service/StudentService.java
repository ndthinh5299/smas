package com.ndthinh.smas.service;

import com.ndthinh.smas.domain.Student;
import com.ndthinh.smas.repository.StudentRepository;

import java.util.List;
import java.util.Optional;

import com.ndthinh.smas.security.oauth2.AuthorizationHeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Student}.
 */
@Service
@Transactional
public class StudentService {

    private final Logger log = LoggerFactory.getLogger(StudentService.class);

    private final StudentRepository studentRepository;

    private final AuthorizationHeaderUtil authorizationHeaderUtil;

    private final UserService userService;


    public StudentService(StudentRepository studentRepository, AuthorizationHeaderUtil authorizationHeaderUtil, UserService userService) {
        this.studentRepository = studentRepository;
        this.authorizationHeaderUtil = authorizationHeaderUtil;
        this.userService = userService;
    }

    /**
     * Save a student.
     *
     * @param student the entity to save.
     * @return the persisted entity.
     */
    public Student save(Student student) {
        log.debug("Request to save Student : {}", student);
        return studentRepository.save(student);
    }

    /**
     * Partially update a student.
     *
     * @param student the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Student> partialUpdate(Student student) {
        log.debug("Request to partially update Student : {}", student);

        return studentRepository
            .findById(student.getId())
            .map(
                existingStudent -> {
                    if (student.getStudentCode() != null) {
                        existingStudent.setStudentCode(student.getStudentCode());
                    }
                    if (student.getName() != null) {
                        existingStudent.setName(student.getName());
                    }
                    if (student.getBirthday() != null) {
                        existingStudent.setBirthday(student.getBirthday());
                    }
                    if (student.getAddress() != null) {
                        existingStudent.setAddress(student.getAddress());
                    }
                    if (student.getEmail() != null) {
                        existingStudent.setEmail(student.getEmail());
                    }
                    if (student.getHomePhone() != null) {
                        existingStudent.setHomePhone(student.getHomePhone());
                    }
                    if (student.getParentId() != null) {
                        existingStudent.setParentId(student.getParentId());
                    }

                    return existingStudent;
                }
            )
            .map(studentRepository::save);
    }

    /**
     * Get all the students.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Student> findAll(Pageable pageable) {
        Optional<String> s = authorizationHeaderUtil.getAuthorizationHeader();
        List<String> stringList = userService.getAuthorities();
        log.debug("Request to get all Students");
        return studentRepository.findAll(pageable);
    }

    /**
     * Get one student by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Student> findOne(Long id) {
        log.debug("Request to get Student : {}", id);
        return studentRepository.findById(id);
    }

    /**
     * Delete the student by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Student : {}", id);
        studentRepository.deleteById(id);
    }
}
