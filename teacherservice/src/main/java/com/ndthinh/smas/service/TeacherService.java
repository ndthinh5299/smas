package com.ndthinh.smas.service;

import com.ndthinh.smas.domain.Teacher;
import com.ndthinh.smas.repository.TeacherRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Teacher}.
 */
@Service
@Transactional
public class TeacherService {

    private final Logger log = LoggerFactory.getLogger(TeacherService.class);

    private final TeacherRepository teacherRepository;

    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    /**
     * Save a teacher.
     *
     * @param teacher the entity to save.
     * @return the persisted entity.
     */
    public Teacher save(Teacher teacher) {
        log.debug("Request to save Teacher : {}", teacher);
        return teacherRepository.save(teacher);
    }

    /**
     * Partially update a teacher.
     *
     * @param teacher the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Teacher> partialUpdate(Teacher teacher) {
        log.debug("Request to partially update Teacher : {}", teacher);

        return teacherRepository
            .findById(teacher.getId())
            .map(
                existingTeacher -> {
                    if (teacher.getName() != null) {
                        existingTeacher.setName(teacher.getName());
                    }
                    if (teacher.getBirthday() != null) {
                        existingTeacher.setBirthday(teacher.getBirthday());
                    }
                    if (teacher.getAddress() != null) {
                        existingTeacher.setAddress(teacher.getAddress());
                    }
                    if (teacher.getEmail() != null) {
                        existingTeacher.setEmail(teacher.getEmail());
                    }
                    if (teacher.getPhone() != null) {
                        existingTeacher.setPhone(teacher.getPhone());
                    }
                    if (teacher.getDerpartment() != null) {
                        existingTeacher.setDerpartment(teacher.getDerpartment());
                    }
                    if (teacher.getGrade() != null) {
                        existingTeacher.setGrade(teacher.getGrade());
                    }
                    if (teacher.getUniversity() != null) {
                        existingTeacher.setUniversity(teacher.getUniversity());
                    }

                    return existingTeacher;
                }
            )
            .map(teacherRepository::save);
    }

    /**
     * Get all the teachers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Teacher> findAll(Pageable pageable) {
        log.debug("Request to get all Teachers");
        return teacherRepository.findAll(pageable);
    }

    /**
     * Get one teacher by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Teacher> findOne(Long id) {
        log.debug("Request to get Teacher : {}", id);
        return teacherRepository.findById(id);
    }

    /**
     * Delete the teacher by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Teacher : {}", id);
        teacherRepository.deleteById(id);
    }
}
